#!/bin/sh +x
docker rm -f api-person;
PROJECT_VERSION=$(ls target/*.jar | cut -d'-' -f 3)
echo "PROJECT VERSION: api-person-${PROJECT_VERSION}"

docker run -d --name api-person -p 8080:8080 api-person:${PROJECT_VERSION}
