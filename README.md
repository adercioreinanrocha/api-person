# Documentacao

## Pré requisito para ambientação
- Ter o java 8
- Ter instalado o Docker
- Ter instalado o maven

## Tecnologias/ferramentas utilizadas
- GIT
- Java 8
- Maven > 3.3
- Spring boot 2
- Spring Data Rest
- Docker
- Swagger 2

## Baixando o projeto do github
$ git clone url


## Como executar os testes:
> $ cd api-person/
>
> $ mvn clean test

###### OBS: Os Testes usam o banco h2 mockado.

## Subindo a aplicação local apontando para o banco Local (H2).
> $ mvn clean package
>
> $ cd target/
>
> $ java -jar *.jar


## Subindo a aplicação local com profile Production apontando para o banco Mysql (Docker)

> $ cd api-person/
>
> $ docker rm -f api-database; docker run --name api-database -p 3306:3306 -v ${PWD}/scripts/schema.sql:/docker-entrypoint-initdb.d/schema.sql -e MYSQL_ROOT_PASSWORD=root123 -e MYSQL_DATABASE=api-person mysql:5.7

> $ mvn clean package
>
> $ cd target/
>
> $ java -jar -Dspring.profiles.active=production *.jar


## Subindo a aplicação em um container docker.
> $ mvn clean package

> $ bash build.sh

> $ bash run.sh

> $ docker logs -f api-person

## Caso queira utilizar o POSTMAN para realizar testes manuais:
 O arquivo encontra-se em:
  > "postman/api-person.postam_collection.json"


## Acesso a documentacao do endpoint via swagger 
http://localhost:8080/swagger-ui.html
