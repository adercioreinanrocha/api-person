package br.com.api.services;

import br.com.api.entity.Customer;
import br.com.api.infrastructure.exceptions.BusinessException;
import br.com.api.infrastructure.exceptions.EntityNotFoundException;
import br.com.api.repository.AddressRepository;
import br.com.api.repository.CustomerRepository;
import br.com.api.repository.dto.AddressResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private final CustomerRepository repository;
    private final AddressRepository addressRepository;

    @Autowired
    public CustomerService(final CustomerRepository repository, final AddressRepository addressRepository){
        this.repository = repository;
        this.addressRepository = addressRepository;
    }


    public Customer findById(final Long id){

        List<AddressResponse> personResponse = addressRepository.findAddressById(id);

        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Customer Not Found!"));
    }

    public Customer save(final Customer customer){
        return repository.save(customer);
    }

    public Customer update(final Customer customerRequest){
        Customer customer = findById(customerRequest.getId());
        BeanUtils.copyProperties(customerRequest, customer);
        return repository.save(customerRequest);
    }

    public Customer delete(final Long id){
        Customer customer = findById(id);
        repository.delete(customer);
        return customer;
    }
    public Optional<Customer> findByCpf(final String cpf){

        if("000".equalsIgnoreCase(cpf)){
            throw new BusinessException("CPF não pode ser zero!");
        }

        return Optional.ofNullable(repository.findByCpf(cpf));
    }

    public Page<Customer> findByName(final String name, final Pageable pageable){
        return repository.findByNameContainingIgnoreCase(name, pageable);
    }
}
