package br.com.api.infrastructure.log;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public interface Path {

	List<String> IGNORABLES = Collections.unmodifiableList(Arrays.asList("/health", "/check", "/info", "/metrics", "/mappings", "/caches", "/error", "/favicon.ico" , "/prometheus"));

	static String removeExceedingSlashes(String uri) {
		return uri.replaceAll("[/]{2,}", "/").replaceAll("[/]$","");
	}

}
