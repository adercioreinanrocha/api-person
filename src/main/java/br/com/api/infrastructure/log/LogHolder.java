package br.com.api.infrastructure.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogHolder {

    public static Logger getLogger(Object o) {
        return getLogger(o.getClass());
    }

    public static Logger getLogger(Class<?> c) {
        return getLogger(c.getName());
    }

    public static Logger getLogger(String loggerName) {
        return LoggerFactory.getLogger(loggerName);
    }


}
