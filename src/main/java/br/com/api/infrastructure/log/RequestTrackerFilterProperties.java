package br.com.api.infrastructure.log;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RequestTrackerFilterProperties {

    @Value(value = "${http.request-tracker.enable:true}")
    private boolean isRequestTrackerEnabled;

    @Value(value = "${http.request-tracker.forIgnorable.enable:false}")
    private boolean isRequestTrackerForIngorablePathsEnabled;

    @Value(value = "${http.request-tracker.parameters.enabled:false}")
    private boolean isRequestParametersEnabled;

    @Value(value = "${http.request-tracker.parameters:false}")
    private boolean isRequestParametersSetted;

    @Value(value = "${http.request-tracker.parameters.toSanitizeRegex:.*(pass|cpf|cnpj|credit|debit|rg).*}")
    private String requestParametersToSanitizeRegex;

    @Value(value = "${http.request-tracker.headers:false}")
    private boolean isRequestHeadersSetted;

    @Value(value = "${http.request-tracker.headers.enabled:false}")
    private boolean isRequestHeadersEnabled;

    @Value(value = "#{T(java.util.Arrays).asList('${http.request-tracker.pathsToIgnore:}')}")
    private List<String> pathsToIgnore;

    public boolean isRequestTrackerEnabled() {
        return isRequestTrackerEnabled;
    }

    public void setRequestTrackerEnabled(boolean isRequestTrackerEnabled) {
        this.isRequestTrackerEnabled = isRequestTrackerEnabled;
    }

    public boolean isRequestTrackerForIngorablePathsEnabled() {
        return isRequestTrackerForIngorablePathsEnabled;
    }

    public void setRequestTrackerForIngorablePathsEnabled(boolean isRequestTrackerForIngorablePathsEnabled) {
        this.isRequestTrackerForIngorablePathsEnabled = isRequestTrackerForIngorablePathsEnabled;
    }

    public void setRequestParametersEnabled(boolean isRequestParametersEnabled) {
        isRequestParametersEnabled = isRequestParametersEnabled;
    }

    public void setRequestParametersSetted(boolean isRequestParametersSetted) {
        this.isRequestParametersSetted = isRequestParametersSetted;
    }

    public String getRequestParametersToSanitizeRegex() {
        return requestParametersToSanitizeRegex;
    }

    public void setRequestParametersToSanitizeRegex(String requestParametersToSanitizeRegex) {
        this.requestParametersToSanitizeRegex = requestParametersToSanitizeRegex;
    }

    public void setRequestHeadersSetted(boolean isRequestHeadersSetted) {
        this.isRequestHeadersSetted = isRequestHeadersSetted;
    }

    public void setRequestHeadersEnabled(boolean isRequestHeadersEnabled) {
        isRequestHeadersEnabled = isRequestHeadersEnabled;
    }

    public List<String> getPathsToIgnore() {
        return pathsToIgnore;
    }

    public void setPathsToIgnore(List<String> pathsToIgnore) {
        this.pathsToIgnore = pathsToIgnore;
    }


    public boolean isRequestParametersEnabled(){

        return isRequestParametersSetted || isRequestParametersEnabled;

    }

    public boolean isRequestHeadersEnabled() {

        return isRequestHeadersSetted || isRequestHeadersEnabled;

    }

}
