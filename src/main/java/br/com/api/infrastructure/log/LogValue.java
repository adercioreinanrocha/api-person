package br.com.api.infrastructure.log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogValue {

    public static final String ASSIGNMENT_OPERATOR = "=";

    public static final String SPACE = " ";

    public static final String COMMA = ",";

    public static final String QUOTES = "'";

    public static final String DOUBLE_QUOTES = "\"";

    public static String sanitize(String regex, String key, String value) {
        Matcher matcher = Pattern.compile(regex, Pattern.CASE_INSENSITIVE).matcher(key);
        if (matcher.find()) {
            return "*";
        }
        return value;
    }

    public static String wrapWithQuotes(String value) {
        return wrap(value, QUOTES);
    }

    public static String wrapWithDoubleQuotes(String value) {
        return wrap(value, DOUBLE_QUOTES);
    }

    private static String wrap(String value, String wrapChar) {
        return (value != null && (value.contains(SPACE) || value.contains(ASSIGNMENT_OPERATOR) || value.contains(COMMA) || value.contains(DOUBLE_QUOTES)) ? wrapChar + value + wrapChar : value);
    }
}
