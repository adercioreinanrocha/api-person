package br.com.api.infrastructure.log;

public enum LogLayer {

    VIEW, INFRA;

    public String code() {
        return this.name().toLowerCase();
    }
}
