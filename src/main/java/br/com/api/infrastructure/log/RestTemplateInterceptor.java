package br.com.api.infrastructure.log;

import io.micrometer.core.instrument.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

import static br.com.api.infrastructure.log.LogLayer.INFRA;
import static br.com.api.infrastructure.log.LogLineBuilder.logLine;

public class RestTemplateInterceptor implements ClientHttpRequestInterceptor {

    private static final String HTTP_METHOD = "httpMethod";
    private static final String URI = "uri";
    private static final String HTTP_STATUS = "httpStatus";
    private static final String REQUEST_ID = "requestId";
    private static final String ERROR_MESSAGE = "message";

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());


    @Override
    public ClientHttpResponse intercept(
            HttpRequest request,
            byte[] body,
            ClientHttpRequestExecution execution) throws IOException {

        final Instant start = Instant.now();
        ClientHttpResponse response = execution.execute(request, body);
        final long elapsed = Duration.between(start, Instant.now()).toMillis();

        logRequest(request, response, elapsed);

        return response;
    }


    private void logRequest(final HttpRequest request, final ClientHttpResponse response, final long elapsed) {
        final List<String> requestIdHeader = request.getHeaders().get("requestId");
        final String requestId = (requestIdHeader != null) ? requestIdHeader.get(0) : null;

        final String httpMethod = request.getMethod().name();
        final String uri = request.getURI().toString();

        final LogLineBuilder logLineBuilder = logLine(INFRA)
                .withItem(HTTP_METHOD, httpMethod)
                .withItem(URI, uri)
                .withItem(REQUEST_ID, requestId)
                .withElapsedTime(elapsed);

        try {
            final HttpStatus httpStatus = response.getStatusCode();

            logLineBuilder.withItem(HTTP_STATUS, String.valueOf(httpStatus.value()));

            if (httpStatus.equals(HttpStatus.CONFLICT)) {
                LOGGER.warn(logLineBuilder.build());
            } else if (httpStatus.isError()) {
                String errorObject = IOUtils.toString(response.getBody(), StandardCharsets.UTF_8).replace("\n", "").trim().replaceAll(" +", " ");
                logLineBuilder.withItem(ERROR_MESSAGE, errorObject);
                LOGGER.error(logLineBuilder.build());
            } else {
                LOGGER.info(logLineBuilder.build());
            }
        } catch (Exception e) {
            LOGGER.error(logLineBuilder
                .withException(e)
                .build());
        }
    }
}
