package br.com.api.infrastructure.log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.UUID;

public class RequestId {

    private static final ThreadLocal<String> id = new ThreadLocal<String>();

    public static final String HEADER_REQUEST_ID_KEY = "X-REQUEST-ID";

    public static String get() {
        return id.get();
    }

    public static void set(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        storeOrGenerate(httpRequest.getHeader(RequestId.HEADER_REQUEST_ID_KEY));
        httpResponse.addHeader(RequestId.HEADER_REQUEST_ID_KEY, get());
    }

    private static void storeOrGenerate(String requestId) {
        if (Optional.ofNullable(requestId).isPresent()) {
            id.set(requestId);
        } else {
            id.set(UUID.randomUUID().toString());
        }
    }
}
