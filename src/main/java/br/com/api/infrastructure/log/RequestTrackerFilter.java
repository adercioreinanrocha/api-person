package br.com.api.infrastructure.log;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StopWatch;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.WebUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static br.com.api.infrastructure.log.LogLineBuilder.logLine;
import static br.com.api.infrastructure.log.LogValue.*;
import static br.com.api.infrastructure.log.Path.removeExceedingSlashes;
import static org.springframework.util.StringUtils.isEmpty;

@Component
@Order(1)
public class RequestTrackerFilter implements Filter {

    private RequestTrackerFilterProperties requestTrackerFilterProperties;

    public RequestTrackerFilter(RequestTrackerFilterProperties requestTrackerFilterProperties) {
        this.requestTrackerFilterProperties = requestTrackerFilterProperties;
    }

    public void init(FilterConfig config) throws ServletException {
        LogHolder.getLogger(this).debug("initialization");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        if (requestTrackerFilterProperties.isRequestTrackerEnabled()) {
            final StopWatch watch = new StopWatch();
            watch.start();
            final HttpServletRequest httpRequest = (HttpServletRequest) request;
            final HttpServletResponse httpResponse = (HttpServletResponse) response;
            RequestId.set(httpRequest, httpResponse);
            Exception possibleException = null;
            try {
                chain.doFilter(request, response);
            } catch (Exception e) {
                possibleException = e;
                throw e;
            } finally {
                watch.stop();
                if (this.isToLogAccess(httpRequest)) {
                    this.logAccess(watch, httpRequest, httpResponse, possibleException);
                }
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    private boolean isToLogAccess(HttpServletRequest httpRequest) {
        if (requestTrackerFilterProperties.isRequestTrackerForIngorablePathsEnabled()) {
            return Boolean.TRUE;
        } else {
            return !this.isAnIgnorablePath(httpRequest.getRequestURI());
        }
    }

    public void destroy() {
        LogHolder.getLogger(this).debug("destruction");
    }

    private void logAccess(final StopWatch watch, HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                           Throwable e) {
        LogHolder.getLogger(this)
            .info(logLine(LogLayer.VIEW)
                .withHttpRequest(httpRequest)
                .withUri(this.extractHttpRequestUri(httpRequest))
                .withHttpRequestHeaders(this.extractHttpRequestHeaders(httpRequest))
                .withHttpResponse(httpResponse)
                .withElapsedTime(watch.getTotalTimeMillis())
                .withException(e)
                .build());
    }

    private Map<String, String> extractHttpRequestHeaders(HttpServletRequest httpRequest) {
        Map<String, String> headers = new LinkedHashMap<>();
        if (requestTrackerFilterProperties.isRequestHeadersEnabled()) {
            Enumeration<String> headerNames = httpRequest.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String key = headerNames.nextElement();
                String value = httpRequest.getHeader(key);
                headers.put(key, wrapWithDoubleQuotes(value));
            }
        }
        return headers;
    }

    private String extractHttpRequestUri(HttpServletRequest httpRequest) {

        if (requestTrackerFilterProperties.isRequestParametersEnabled()) {

            UriComponentsBuilder uriComponentsBuilder =  UriComponentsBuilder.fromPath(httpRequest.getServletPath());

            if(hasMatrixVariable(httpRequest)){
                setParametersWithMatrixVariables(httpRequest, uriComponentsBuilder);
            }

            if(hasQueryParameter(httpRequest)) {
                setParametersWithQueryParameters(httpRequest, uriComponentsBuilder);
            }

            return uriComponentsBuilder.build().toString();
        }else{
            return httpRequest.getRequestURI();
        }
    }

    private boolean hasMatrixVariable(HttpServletRequest httpRequest) {
        return httpRequest.getRequestURI().contains(";");
    }

    private void setParametersWithQueryParameters(HttpServletRequest httpRequest, UriComponentsBuilder uriComponentsBuilder) {

        ArrayList<String> listOfKeys = new ArrayList<String>(httpRequest.getParameterMap().keySet());

        for (String key : listOfKeys) {
            String value = null;
            String[] values = httpRequest.getParameterMap().get(key);
            if (values.length > 1) {
                value = String.join(COMMA, values);
            } else {
                value = values[0];
            }
            value  = sanitize(requestTrackerFilterProperties.getRequestParametersToSanitizeRegex(), key, wrapWithDoubleQuotes(value));
            uriComponentsBuilder.queryParam(key, value);
        }
    }

    private void setParametersWithMatrixVariables(HttpServletRequest httpRequest , UriComponentsBuilder uriComponentsBuilder) {

        String matrixVariables = httpRequest.getRequestURI().replaceFirst(httpRequest.getServletPath(),"");
        MultiValueMap<String, String> matrixVariablesParsed = WebUtils.parseMatrixVariables(matrixVariables);

        StringBuilder matrixVariableAppender = new StringBuilder();

        for(String key : matrixVariablesParsed.keySet()){
            String value = null;
            List<String> values = matrixVariablesParsed.get(key);
            if (values.size() > 1) {
                value = String.join(COMMA, values);
            } else {
                value = values.get(0);
            }
            value = sanitize(requestTrackerFilterProperties.getRequestParametersToSanitizeRegex(), key, wrapWithDoubleQuotes(value));

            matrixVariableAppender.append(";");
            matrixVariableAppender.append(key);
            matrixVariableAppender.append("=");
            matrixVariableAppender.append(value);
        }
        uriComponentsBuilder.path(matrixVariableAppender.toString());
    }

    private boolean hasQueryParameter(HttpServletRequest httpRequest) {
        return !httpRequest.getParameterMap().isEmpty();
    }

    private boolean isAnIgnorablePath(String path) {
        if (!isEmpty(path)) {
            String strippedPath = removeExceedingSlashes(path);
            return Path.IGNORABLES.contains(strippedPath) || requestTrackerFilterProperties.getPathsToIgnore().stream().anyMatch(strippedPath::contains);
        }
        return false;
    }

}
