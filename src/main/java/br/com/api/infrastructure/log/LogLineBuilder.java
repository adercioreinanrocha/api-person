package br.com.api.infrastructure.log;

import io.micrometer.core.instrument.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import static br.com.api.infrastructure.log.LogValue.ASSIGNMENT_OPERATOR;
import static br.com.api.infrastructure.log.LogValue.wrapWithQuotes;

public class LogLineBuilder {

    private Map<String, String> items = new LinkedHashMap<>();

    private LogLineBuilder(LogLayer layer) {
        this.withLayer(layer);
    }

    public static LogLineBuilder logLine(LogLayer layer) {
        return new LogLineBuilder(layer);
    }

    public LogLineBuilder withHttpStatus(String httpStatusCode) {
        return this.withItem("httpStatus", httpStatusCode);
    }

    public LogLineBuilder withHttpMethod(String httpMethod) {
        return this.withItem("httpMethod", httpMethod);
    }

    public LogLineBuilder withRequestId() {
        return this.withItem("requestId", RequestId.get());
    }

    public LogLineBuilder withRequestId(String requestId) {
        if (requestId != null) {
            return this.withItem("requestId", requestId);
        }
        return this.withRequestId();
    }

    public LogLineBuilder withUri(String uri) {
        return this.withItem("uri", uri);
    }

    public LogLineBuilder withRemoteHost(String remoteHost) {
        return this.withItem("remoteHost", remoteHost);
    }

    public LogLineBuilder withBrowserIp(String browserIp) {
        return this.withItem("browserIp", browserIp);
    }

    public LogLineBuilder withHttpRequest(HttpServletRequest httpRequest) {
        this.withHttpMethod(httpRequest.getMethod());
        this.withRequestId();
        this.withRemoteHost(this.getRemoteHost(httpRequest));
        this.withBrowserIp(this.getBrowserIp(httpRequest));
        return this;
    }

    public LogLineBuilder withHttpRequestHeaders(Map<String, String> requestHeaders) {
        if (requestHeaders.size() > 0) {
            String requestHeadersAsString = this.mapAsString(requestHeaders);
            return this.withItem("headers", requestHeadersAsString);
        } else {
            return this;
        }
    }

    public LogLineBuilder withHttpResponse(HttpServletResponse httpResponse) {
        this.withHttpStatus(String.valueOf(httpResponse.getStatus()));
        return this;
    }

    public LogLineBuilder withElapsedTime(long elapsedTime) {
        return this.withItem("elapsedTime", String.valueOf(elapsedTime));
    }

    public LogLineBuilder withException(Throwable e) {
        if (e != null) {
            String exception = "class=" + e.getClass().getName();
            if (e.getMessage() != null) {
                exception = exception + ",message=\"" + e.getMessage() + "\"";
            }
            this.withItem("exception", exception);
        }
        return this;
    }

    public LogLineBuilder withBody(String body) {
        if (StringUtils.isNotEmpty(body)) {
            return this.withItem("body", body);
        }
        return this;
    }

    public LogLineBuilder withEvent(String event) {
        return this.withItem("event", event);
    }

    private LogLineBuilder withLayer(LogLayer layer) {
        return this.withItem("layer", layer.code());
    }


    public LogLineBuilder withMethodArguments(Map<String, String> methodArguments) {
        if (methodArguments.size() > 0) {
            String methodArgumentsAsString = this.mapAsString(methodArguments);
            return this.withItem("methodArguments", methodArgumentsAsString);
        } else {
            return this;
        }
    }

    public LogLineBuilder withItem(String name, String value) {
        this.items.put(name, value);
        return this;
    }

    public String buildWithNullValues() {
        return this.build(Boolean.FALSE);
    }

    public String build() {
        return this.build(Boolean.TRUE);
    }

    private String build(boolean stripNullValues) {
        Function<Entry<String, String>, String> itemAsKeyAndValue = e -> e.getKey() + ASSIGNMENT_OPERATOR + wrapWithQuotes(e.getValue());
        return items.entrySet().parallelStream()
                .filter(e -> !stripNullValues || e.getValue() != null && !e.getValue().equals("null"))
                .map(itemAsKeyAndValue)
                .collect(Collectors.joining(LogValue.SPACE));
    }

    private String mapAsString(Map<String, String> arguments) {
        return arguments.entrySet().parallelStream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining(","));
    }

    private String getBrowserIp(HttpServletRequest httpRequest) {
        return httpRequest.getHeader("X-Browser-Ip");
    }

    private String getRemoteHost(HttpServletRequest httpRequest) {
        String xForwardedFor = httpRequest.getHeader("X-Forwarded-For");
        if (xForwardedFor != null && xForwardedFor.length() > 0 && !xForwardedFor.equals("127.0.0.1")) {
            if (xForwardedFor.indexOf(",") != -1) {
                xForwardedFor = xForwardedFor.substring(xForwardedFor.lastIndexOf(",") + 1);
            }
            return xForwardedFor.trim();
        } else {
            return httpRequest.getRemoteHost();
        }
    }
}
