package br.com.api.infrastructure.actuator;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

@Component
public class Acl {

	private static int DEFAULT_SOCKET_PORT = 80;
	public static String OPENED_CODE = "OK";
	public static String CLOSED_CODE = "NOK";

	public boolean isOpen(String hostAndPort, int socketTimeoutInMilliseconds) {
		String[] hostAndPortArray = hostAndPort.split(":");
		String host = hostAndPortArray[0];
		int port = hostAndPortArray.length > 1 ? Integer.valueOf(hostAndPortArray[1]) : DEFAULT_SOCKET_PORT;
		Socket socket = new Socket();
		try {
			socket.connect(new InetSocketAddress(host, port), socketTimeoutInMilliseconds);
			return true;
		} catch (IOException ex) {
			return false;
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					// ignore
				}
			}
		}
	}
}

