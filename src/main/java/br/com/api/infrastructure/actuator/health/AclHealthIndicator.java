package br.com.api.infrastructure.actuator.health;

import br.com.api.infrastructure.actuator.Acl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AclHealthIndicator implements HealthIndicator {

    @Value(value = "${management.health.acl.timeout:1000}")
    private int socketTimeoutInMilliseconds;
    
	private Acl acl;

	@Value(value = "#{T(java.util.Arrays).asList('${management.health.acl:}')}")
	private List<String> dependencies;

	AclHealthIndicator(Acl acl) {
		this.acl = acl;
	}

	@Override
	public Health health() {
		Health.Builder health = Health.up();
		for (String hostAndPort : dependencies) {
			if (acl.isOpen(hostAndPort, socketTimeoutInMilliseconds)) {
				health.withDetail(hostAndPort, Acl.OPENED_CODE);
			} else {
				health.status(Status.DOWN);
				health.withDetail(hostAndPort, Acl.CLOSED_CODE);
			}
		}
		return health.build();
	}

}
