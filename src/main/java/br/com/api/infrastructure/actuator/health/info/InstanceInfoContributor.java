package br.com.api.infrastructure.actuator.health.info;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class InstanceInfoContributor implements InfoContributor {

    @Value(value = "${server.address:}")
    private String bindHost;

    @Value(value = "${server.port}")
    private String bindPort;

    @Override
    public void contribute(Builder builder) {

        Map<String, Object> instance = new LinkedHashMap<>();

        try {
            String bindIp = null;
            if ("".equals(bindHost)) {
                bindHost = "*";
                bindIp = "*";
            } else {
                bindIp = InetAddress.getByName(bindHost).getHostAddress();
            }
            instance.put("host", InetAddress.getLocalHost().getHostName());
            instance.put("bindIp", bindIp);
            instance.put("bindHost", bindHost);
            instance.put("bindPort", bindPort);
        } catch (UnknownHostException e) {
        }

        builder.withDetail("instance", instance);
    }

}
