package br.com.api.infrastructure.actuator;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class Dependency {

	public static String HEALTH_CODE = "UP";
	public static String UNHEALTH_CODE = "DOWN";

	public boolean isHealthWithRetry(String url, Integer httpConnectionRetry, Integer httpConnectionRetryInterval, Integer httpConnectionTimeout) {
		for (int retry = 0; retry <= httpConnectionRetry; retry++) {
			if (this.isHealth(url, httpConnectionTimeout)) {
				return true;
			}
			try {
				Thread.sleep(httpConnectionRetryInterval);
			} catch (InterruptedException e) {
				continue;
			}
		}
		return false;
	}

	public boolean isHealth(String url, Integer httpConnectionTimeout) {
		Integer httpStatusCode = this.getHttpStatusCode(url, httpConnectionTimeout);
		if (HttpServletResponse.SC_OK != httpStatusCode) {
			return false;
		}
		return true;
	}

	private Integer getHttpStatusCode(String url, Integer httpConnectionTimeout) {
		Integer code = null;
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(httpConnectionTimeout);
			connection.connect();
			code = connection.getResponseCode();
		} catch (Exception e) {
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return code;
	}
}
