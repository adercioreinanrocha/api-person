package br.com.api.infrastructure.actuator.health;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.actuate.metrics.MetricsEndpoint;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class MemoryHealthIndicator implements HealthIndicator {

	private final static String MESSAGE_KEY = "Message";
	private final static String MESSSAGE_WHEN_MAX_USED_MEMORY_ACCEPTED_REACH = "Memory used greater than {0}%";

	@Value("${management.health.memory.maxUsedAcceptablePercentage:95}")
	private Integer maxUsedAcceptablePercentage;

	private MetricsEndpoint metricsEndpoint;

	public MemoryHealthIndicator(MetricsEndpoint metricsEndpoint) {
		this.metricsEndpoint = metricsEndpoint;
	}

	@Override
	public Health health() {
		Double jvmUsedMemory = this.getMetric("jvm.memory.used");
		Double jvmMaxMemory = this.getMetric("jvm.memory.max");
		Double jvmFreeMemory = (jvmMaxMemory - jvmUsedMemory);

		Health.Builder health = Health.up();
		health.withDetail("Total", jvmMaxMemory);
		health.withDetail("Used", jvmUsedMemory);
		health.withDetail("Free", jvmFreeMemory);

		if (this.isMemoryAvailabilityNotAcceptable(jvmUsedMemory, jvmMaxMemory)) {
			health.status(Status.DOWN);
			health.withDetail(MESSAGE_KEY, MessageFormat.format(MESSSAGE_WHEN_MAX_USED_MEMORY_ACCEPTED_REACH, maxUsedAcceptablePercentage));
		}
		return health.build();
	}

	private Double getMetric(String metricName) {
		return metricsEndpoint.metric(metricName, null).getMeasurements().get(0).getValue();
	}

	private boolean isMemoryAvailabilityNotAcceptable(Double jvmUsedMemory, Double jvmMaxMemory) {
		return ((jvmUsedMemory / jvmMaxMemory) * 100 >= maxUsedAcceptablePercentage);
	}
}
