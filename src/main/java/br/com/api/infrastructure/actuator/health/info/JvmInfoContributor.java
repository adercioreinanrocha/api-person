package br.com.api.infrastructure.actuator.health.info;

import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

@Component
public class JvmInfoContributor implements InfoContributor {

    @Override
    public void contribute(Builder builder) {
        Map<String, Object> jvm = new LinkedHashMap<>();

        RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
        jvm.put("vendor", runtime.getSpecVendor());
        jvm.put("name", runtime.getVmName());
        jvm.put("version", runtime.getSpecVersion() + " " + runtime.getVmVersion());
        jvm.put("arguments", runtime.getInputArguments());
        
        List<String> classpath = new ArrayList<>();
        
        for (URL url : ((URLClassLoader) ClassLoader.getSystemClassLoader()).getURLs()) {
            classpath.add(url.getFile());
        }
        Collections.sort(classpath);
        jvm.put("classpath", classpath);
        
        builder.withDetail("jvm", jvm);
    }

}

