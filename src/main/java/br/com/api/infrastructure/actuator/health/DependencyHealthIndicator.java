package br.com.api.infrastructure.actuator.health;

import br.com.api.infrastructure.actuator.Dependency;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

@Component
public class DependencyHealthIndicator implements HealthIndicator {

	@Value(value = "#{T(java.util.Arrays).asList('${management.health.dependency.urls:}')}")
	private List<String> urls;

	@Value(value = "${management.health.dependency.httpConnectionTimeout:1000}")
	private Integer httpConnectionTimeout;

	@Value(value = "${management.health.dependency.httpConnectionRetry:3}")
	private Integer httpConnectionRetry;

	@Value(value = "${management.health.dependency.httpConnectionRetryInterval:100}")
	private Integer httpConnectionRetryInterval;

	private Dependency dependency;

	DependencyHealthIndicator(Dependency dependency) {
		this.dependency = dependency;
	}

	@Override
	public Health health() {
		Health.Builder health = Health.up();
		for (String url : urls) {
			if (!StringUtils.isEmpty(url)) {
				if (dependency.isHealthWithRetry(url, httpConnectionRetry, httpConnectionRetry, httpConnectionTimeout)) {
					health.withDetail(url, Dependency.HEALTH_CODE);
				} else {
					health.status(Status.DOWN);
					health.withDetail(url, Dependency.UNHEALTH_CODE);
				}
			}
		}
		return health.build();
	}
}
