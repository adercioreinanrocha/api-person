package br.com.api.endpoints.validator;

import br.com.api.entity.Customer;
import br.com.api.infrastructure.exceptions.ValidationException;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class CustomerValidator implements Validator<Customer> {

    @Override
    public void validate(Customer customer){
        if(StringUtils.isEmpty(customer.getName())){
            throw new ValidationException("name is mandatory");
        }

        if(StringUtils.isEmpty(customer.getCpf())){
            throw new ValidationException("cpf is mandatory");
        }
    }

    public void validate(String cpf, String name) {
        if(StringUtils.isEmpty(cpf) && StringUtils.isEmpty(name)){
            throw new ValidationException("cpf or name is mandatory");
        }
        if(StringUtils.isNotEmpty(cpf) && StringUtils.isNotEmpty(name)){
            throw new ValidationException("fill only the param cpf or name");
        }
    }
}
