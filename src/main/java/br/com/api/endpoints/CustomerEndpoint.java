package br.com.api.endpoints;

import br.com.api.endpoints.validator.CustomerValidator;
import br.com.api.entity.Customer;
import br.com.api.services.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static io.micrometer.core.instrument.util.StringUtils.isNotEmpty;

@RestController
@RequestMapping(value = "/rs", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "Customer")
public class CustomerEndpoint {

    private final CustomerService service;
    private final CustomerValidator validator;

    @Autowired
    public CustomerEndpoint(final CustomerService service, final CustomerValidator validator){
        this.service = service;
        this.validator = validator;
    }

    @ApiOperation(value = "Consultar cliente(s) por CPF ou Nome")
    @GetMapping(value = "/customers{cpf}{name}")
    public ResponseEntity<Page<Customer>> search(@MatrixVariable(required = false) final String cpf,
                                 @MatrixVariable(required = false) final String name,
                                 final Pageable pageable){
        validator.validate(cpf, name);
        Page<Customer> customerPageable = null;

        if(isNotEmpty(cpf)) {
            List<Customer> customers = new ArrayList<>();
            service.findByCpf(cpf).ifPresent(customers::add);
            customerPageable = new PageImpl<>(customers, pageable, customers.size());;
        }

        if (isNotEmpty(name)) {
            customerPageable = service.findByName(name, pageable);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(customerPageable);
    }

    @ApiOperation(value = "Consultar cliente pelo Identificador")
    @GetMapping(value = "/customers/{id}")
    public ResponseEntity<Customer> findById(@PathVariable Long id){
       // String teste = null;
       // teste.toString();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.findById(id));
    }

    @ApiOperation(value = "Criar um novo Cliente")
    @PostMapping(value = "/customers")
    public ResponseEntity<Customer> save(@RequestBody Customer customer){
        validator.validate(customer);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(service.save(customer));
    }

    @ApiOperation(value = "Alterar um Cliente existente")
    @PutMapping(value = "/customers/{id}")
    public ResponseEntity<Customer> update(@PathVariable Long id, @RequestBody Customer customer){
        customer.setId(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.update(customer));
    }

    @ApiOperation(value = "Excluir um cliente existente")
    @DeleteMapping(value = "/customers/{id}")
    public ResponseEntity<Customer> delete(@PathVariable Long id){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.delete(id));
    }
}
