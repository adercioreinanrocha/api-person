package br.com.api.repository;

import br.com.api.repository.dto.AddressResponse;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class AddressRepository {

    @Value("${address.host}")
    private String addressHost;

    @Autowired
    private RestTemplate restTemplate;

    public List<AddressResponse> findAddressById(final Long customerId){
        final String url = addressHost + "/rs/customers/" + customerId + "/addresses";
        ResponseEntity<List<AddressResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<AddressResponse>>() {
        });
        if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            return responseEntity.getBody();
        }

        if(HttpStatus.NOT_FOUND.equals(responseEntity.getStatusCode())) {
            return Lists.newArrayList();
        }
        throw new InternalError("Error ao acessar " + url+ " with return_body=" + responseEntity.getBody());
    }
}
