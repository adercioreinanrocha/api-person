package br.com.api.repository;

import br.com.api.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findByCpf(final String cpf);

    Page<Customer> findByNameContainingIgnoreCase(final String name, Pageable pageable);
}
